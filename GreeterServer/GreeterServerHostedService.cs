using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Hosting;

namespace GreeterServer {
	public class GreeterServerHostedService : IHostedService {
		private Server _server;

		public GreeterServerHostedService(Server server) {
			_server = server;
		}

		public Task StartAsync(CancellationToken cancellationToken) {
			_server.Start();
			return Task.CompletedTask;
		}

		public async Task StopAsync(CancellationToken cancellationToken) {
			await _server.ShutdownAsync();
		}
	}
}