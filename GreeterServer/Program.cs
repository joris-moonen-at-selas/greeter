using System;
using System.Threading.Tasks;
using Grpc.Core;
using Helloworld;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace GreeterServer {
	class Program {
		public static async Task Main(string[] args) {
			HostBuilder hostBuilder = new HostBuilder();
			hostBuilder.ConfigureServices((HostBuilderContext, services) => {
				Server server = new Server {
					Services = { Greeter.BindService(new GreeterImpl()) },
						Ports = {
							new ServerPort("0.0.0.0", 50001, ServerCredentials.Insecure)
						}
				};
				services.AddSingleton<Server>(server);
				services.AddSingleton<IHostedService, GreeterServerHostedService>();
			});

			await hostBuilder.RunConsoleAsync();
		}
	}
}