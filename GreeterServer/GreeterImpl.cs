using System;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Helloworld;

namespace GreeterServer {
	public class GreeterImpl : Greeter.GreeterBase {
		public override Task<HelloReply> SayHello(HelloRequest request, ServerCallContext context) {
			Console.WriteLine("Recieved name: " + request.Name);
			HelloReply result = new HelloReply { Message = "Hello " + request.Name };
			Console.WriteLine("Send reply: " + result.Message);
			return Task.FromResult(result);
		}

		public override Task<HelloReply> SayBye(HelloRequest request, ServerCallContext context) {
			return Task.FromResult(new HelloReply { Message = "Bye " + request.Name });
		}

		public async override Task Talk(IAsyncStreamReader<Subject> requestStream, IServerStreamWriter<Subject> responseStream, ServerCallContext context) {
			while (await requestStream.MoveNext(CancellationToken.None)) {
				Subject message = requestStream.Current;
				Console.WriteLine($"Client says at {message.Content}: {message.MessageNumber} ");
				await responseStream.WriteAsync(message);
			}
		}
	}
}