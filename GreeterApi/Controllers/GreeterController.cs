using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Helloworld;
using Microsoft.AspNetCore.Mvc;

namespace GreeterApi.Controllers {
	[Route("api/[controller]")]
	[ApiController]
	public class GreeterController : ControllerBase {
		// GET api/greeter/name
		[HttpGet("{name}")]
		public ActionResult<HelloReply> Get(string name) {
			Console.WriteLine("Recieved name: " + name);
			Channel channel = new Channel("greeterserver:50001", ChannelCredentials.Insecure);

			Greeter.GreeterClient client = new Greeter.GreeterClient(channel);

			HelloRequest request = new HelloRequest { Name = name };
			Console.WriteLine("Sent name: " + request.Name);
			HelloReply reply = client.SayHello(request);
			Console.WriteLine("Recieved reply: " + reply.Message);
			return reply;
		}
	}
}