using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace GreeterClient {
	class Program {
		private static readonly HttpClient client = new HttpClient();

		public static async Task Main(string[] args) {
			Console.Write("Name: ");
			string name = Console.ReadLine();

			client.DefaultRequestHeaders.Accept.Clear();
			client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json"));
			client.DefaultRequestHeaders.Add("User-Agent", ".NET Foundation Repository Reporter");

			var stringTask = client.GetStringAsync("http://localhost/api/greeter/" + name);

			var msg = await stringTask;
			Console.WriteLine(msg);
		}
	}
}